<?php

namespace App\Service;

use App\Entity\AirQuality;
use App\Entity\Station;
use Doctrine\Common\Annotations\AnnotationReader;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AirQualityApi
{
    /**
     * @var string
     */
    protected string $url = 'http://api.gios.gov.pl/pjp-api/rest/';

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    /**
     * @var Serializer
     */
    private Serializer $serializer;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(HttpClientInterface $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->serializer = $this->getSerializer();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    public function fetchAllStations(): array
    {
        $json = $this->request('station/findAll');

        return $this->serializer->deserialize($json, Station::class.'[]', 'json');
    }

    public function fetchStationAirCondition(int $stationId): AirQuality
    {
        $json = $this->request('aqindex/getIndex/' . $stationId);

        return $this->serializer->deserialize($json, AirQuality::class, 'json');
    }

    /**
     * @return array
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function fetchCities(): array
    {
        $cities = [];

        /** @var Station $station */
        foreach($this->fetchAllStations() as $station){
            $cityName = $station->getCity()->getName();
            $cityId = $station->getCity()->getId();

            if(!array_key_exists($cityName,$cities)){
                $cities[$cityId] = $cityName;
            }
        }

        return $cities;
    }

    /**
     * @return array [cityId => [stationsId]
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function fetchCitiesStations(): array
    {
        $citiesStations = [];

        /** @var Station $station */
        foreach ($this->fetchAllStations() as $station) {
            $cityId = $station->getCity()->getId();

            array_key_exists($cityId, $citiesStations)
                ? $citiesStations[$cityId]->addStation($station)
                : $citiesStations[$cityId] = $station->getCity()->addStation($station);
        }

        return $citiesStations;
    }

    /**
     * @param string $path
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function request(string $path): string
    {
        try {
            $response = $this->client->request('GET', $this->url . '/' . $path);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());

            return '';
        }

        return $response->getContent();
    }

    /**
     * @return Serializer
     */
    public function getSerializer(): Serializer
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        return new Serializer([new ArrayDenormalizer, new DateTimeNormalizer, new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter, null, new ReflectionExtractor())],[new JsonEncoder()]);
    }
}