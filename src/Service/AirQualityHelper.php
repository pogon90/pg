<?php


namespace App\Service;

use App\Entity\AirQuality;
use App\Entity\Station;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class AirQualityHelper
{
    /**
     * @var AirQualityApi
     */
    private AirQualityApi $airQualityApi;

    /**
     * @var CacheInterface
     */
    private CacheInterface $airQualityApiCache;
    private const CACHE_DURATION = 3600;

    /**
     * AirConditionHelper constructor.
     * @param AirQualityApi $airConditionApi
     * @param CacheInterface $airQualityApiCache
     */
    public function __construct(AirQualityApi $airConditionApi, CacheInterface $airQualityApiCache)
    {
        $this->airQualityApi = $airConditionApi;
        $this->airQualityApiCache = $airQualityApiCache;
    }

    /**
     * @param int $cityId
     * @return array
     * @throws InvalidArgumentException
     */
    public function getCityAirConditions(int $cityId): array
    {
        return array_map(function(Station $station){
            return $this->getStationAirCondition($station->getId()) ?? null;
        },self::getCitiesStations()[$cityId]->getStations()->toArray() ?? []);
    }

    /**
     * @return array
     * @throws InvalidArgumentException
     */
    public function getCitiesStations(): array
    {
        return $this->airQualityApiCache->get('cities_stations', function (ItemInterface $item) {
            $item->expiresAfter(self::CACHE_DURATION);

            return $this->airQualityApi->fetchCitiesStations();
        });
    }

    /**
     * @param $stationId
     * @return AirQuality
     * @throws InvalidArgumentException
     */
    public function getStationAirCondition($stationId): AirQuality
    {
        return $this->airQualityApiCache->get('station_air_condition_'.$stationId, function (ItemInterface $item) use ($stationId) {
            $item->expiresAfter(self::CACHE_DURATION);

            return $this->airQualityApi->fetchStationAirCondition($stationId);
        });
    }

    /**
     * @return array
     * @throws InvalidArgumentException
     */
    public function getCities(): array
    {
        return $this->airQualityApiCache->get('cities', function (ItemInterface $item){
            $item->expiresAfter(self::CACHE_DURATION);

            return $this->airQualityApi->fetchCities();
        });
    }

    /**
     * @param $data
     * @return string
     */
    public function serialize($data): string
    {
        return $this->airQualityApi->getSerializer()->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
    }
}