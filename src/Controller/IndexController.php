<?php

namespace App\Controller;

use App\Form\CityType;
use App\Service\AirQualityHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @Template
     * @param Request $request
     * @param AirQualityHelper $airQualityHelper
     * @return array
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function index(Request $request, AirQualityHelper $airQualityHelper): array
    {
        $form = $this->createForm(CityType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $stations = $airQualityHelper->getCitiesStations()[$form->get('city')->getData()] ?? [];
            $stationsAirQuality = $airQualityHelper->getCityAirConditions($form->get('city')->getData());

            $stationsJson = $airQualityHelper->serialize($stations);
            $stationsAirQualityJson = $airQualityHelper->serialize($stationsAirQuality);
        }

        return [
            'form' => $form->createView(),
            'stationsJson' => $stationsJson ?? null,
            'stationsAirQualityJson' => $stationsAirQualityJson ?? null,
        ];
    }
}
