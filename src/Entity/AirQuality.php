<?php

namespace App\Entity;

use App\Repository\AirQualityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=AirQualityRepository::class)
 */
class AirQuality
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @SerializedName("stSourceDataDate")
     */
    private $sourceDataDate;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     * @SerializedName("stIndexLevel")
     */
    private $indexLevel;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     * @SerializedName("so2IndexLevel")
     */
    private $so2;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     * @SerializedName("no2IndexLevel")
     */
    private $no2;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     * @SerializedName("coIndexLevel")
     */
    private $co;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     * @SerializedName("pm10IndexLevel")
     */
    private $pm10;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     * @SerializedName("pm25IndexLevel")
     */
    private $pm25;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     * @SerializedName("o3IndexLevel")
     */
    private $o3;

    /**
     * @ORM\ManyToOne(targetEntity=AirQualityIndexLevel::class, inversedBy="airQualities")
     */
    private $c6h6;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getSourceDataDate(): ?\DateTimeInterface
    {
        return $this->sourceDataDate;
    }

    public function setSourceDataDate(\DateTimeInterface $sourceDataDate): self
    {
        $this->sourceDataDate = $sourceDataDate;

        return $this;
    }

    public function getIndexLevel(): ?AirQualityIndexLevel
    {
        return $this->indexLevel;
    }

    public function setIndexLevel(?AirQualityIndexLevel $indexLevel): self
    {
        $this->indexLevel = $indexLevel;

        return $this;
    }

    public function getSo2(): ?AirQualityIndexLevel
    {
        return $this->so2;
    }

    public function setSo2(?AirQualityIndexLevel $so2): self
    {
        $this->so2 = $so2;

        return $this;
    }

    public function getNo2(): ?AirQualityIndexLevel
    {
        return $this->no2;
    }

    public function setNo2(?AirQualityIndexLevel $no2): self
    {
        $this->no2 = $no2;

        return $this;
    }

    public function getCo(): ?AirQualityIndexLevel
    {
        return $this->co;
    }

    public function setCo(?AirQualityIndexLevel $co): self
    {
        $this->co = $co;

        return $this;
    }

    public function getPm10(): ?AirQualityIndexLevel
    {
        return $this->pm10;
    }

    public function setPm10(?AirQualityIndexLevel $pm10): self
    {
        $this->pm10 = $pm10;

        return $this;
    }

    public function getPm25(): ?AirQualityIndexLevel
    {
        return $this->pm25;
    }

    public function setPm25(?AirQualityIndexLevel $pm25): self
    {
        $this->pm25 = $pm25;

        return $this;
    }

    public function getO3(): ?AirQualityIndexLevel
    {
        return $this->o3;
    }

    public function setO3(?AirQualityIndexLevel $o3): self
    {
        $this->o3 = $o3;

        return $this;
    }

    public function getC6h6(): ?AirQualityIndexLevel
    {
        return $this->c6h6;
    }

    public function setC6h6(?AirQualityIndexLevel $c6h6): self
    {
        $this->c6h6 = $c6h6;

        return $this;
    }
}
