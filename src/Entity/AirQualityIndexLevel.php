<?php

namespace App\Entity;

use App\Repository\AirQualityIndexLevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=AirQualityIndexLevelRepository::class)
 */
class AirQualityIndexLevel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("indexLevelName")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=AirQuality::class, mappedBy="indexLevel")
     */
    private $airQualities;

    public function __construct()
    {
        $this->airQualities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|AirQuality[]
     */
    public function getAirQualities(): Collection
    {
        return $this->airQualities;
    }

    public function addAirQuality(AirQuality $airQuality): self
    {
        if (!$this->airQualities->contains($airQuality)) {
            $this->airQualities[] = $airQuality;
            $airQuality->setIndexLevel($this);
        }

        return $this;
    }

    public function removeAirQuality(AirQuality $airQuality): self
    {
        if ($this->airQualities->removeElement($airQuality)) {
            // set the owning side to null (unless already changed)
            if ($airQuality->getIndexLevel() === $this) {
                $airQuality->setIndexLevel(null);
            }
        }

        return $this;
    }
}
