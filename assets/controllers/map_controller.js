import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import {OSM} from 'ol/source';
import { Controller } from 'stimulus';
import {fromLonLat} from "ol/proj";
import {Feature, Overlay} from "ol";
import Point from "ol/geom/Point";
import {Icon, Style} from "ol/style";
import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import marker from './../images/marker.png';

export default class extends Controller {
    static values = {
        lat: String,
        lon: Number,
        stations: String,
        air: String
    }

    static targets = [ "popup" ]

    connect() {
        this.getMarkers();

        const vectorLayer = new VectorLayer({
            source: new VectorSource({
                features: this.getMarkers(),
            }),
        });

        const map = this.initMap(vectorLayer);

        if(this.stationsValue && this.airValue){
            const popup = new Overlay({
                element: this.popupTarget,
                positioning: 'bottom-center',
                stopEvent: false,
                offset: [0, -50],
            });

            map.addOverlay(popup);

            const self = this;
            map.on('click', function (evt) {
                const feature = map.forEachFeatureAtPixel(evt.pixel, function (feature) {
                    return feature;
                });

                if (feature) {
                    const coordinates = feature.getGeometry().getCoordinates();
                    popup.setPosition(coordinates);

                    self.popupTarget.innerHTML = `<ul class="map-popup-list">
                        <li><strong>${feature.get('name')}</strong></li>
                        <li><span>no2:</span> ${feature.get('no2') || 'Brak danych'}</li>
                        <li><span>o3:</span> ${feature.get('o3') || 'Brak danych'}</li>
                        <li><span>pm10:</span> ${feature.get('pm10') || 'Brak danych'}</li>
                        <li><span>pm25:</span> ${feature.get('pm25') || 'Brak danych'}</li>
                        <li><span>so2:</span> ${feature.get('so2') || 'Brak danych'}</li>
                        <li><span>c6h6:</span> ${feature.get('c6h6') || 'Brak danych'}</li>
                        <li><span>st:</span> ${feature.get('st') || 'Brak danych'}</li>
                    </ul>`;
                    self.popupTarget.classList.add('map-popup-visible');
                } else {
                    self.popupTarget.classList.remove('map-popup-visible');
                }
            });
        }
    }

    initMap(markersLayer){
        return new Map({
            target: this.element,
            layers: [
                new TileLayer({
                    source: new OSM()
                }),
                markersLayer
            ],
            view: new View({
                center: fromLonLat([this.lonValue,this.latValue]),
                zoom: 6
            })
        });
    }

    getMarkers(){
        const markers = [];

        if(this.stationsValue && this.airValue){
            const city = JSON.parse(this.stationsValue);
            const airQualityStations = JSON.parse(this.airValue);

            for(const station of city.stations){
                markers.push(this.getMarker(station, airQualityStations));
            }
        }

        return markers;
    }

    getMarker(station,airQualityStations){
        const airQuality = airQualityStations.find(airQualityStation => airQualityStation.id ===station.id);
        let params = {};

        if(airQuality){
            params = this.getAirQualityParams(airQuality);
        }

        const iconFeature = new Feature({
            geometry: new Point(fromLonLat([station.gegrLon, station.gegrLat])),
            name: station.stationName ? station.stationName : '',
            addressStreet: station.addressStreet,
            ...params
        });

        const iconStyle = new Style({
            image: new Icon({
                anchor: [0.5, 46],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                src: marker,
            }),
        });

        iconFeature.setStyle(iconStyle);

        return iconFeature;
    }

    getAirQualityParams(airQuality){
        const params = {};

        if(airQuality.no2IndexLevel){
            params['no2'] = airQuality.no2IndexLevel.indexLevelName;
        }

        if(airQuality.o3IndexLevel){
            params['o3'] = airQuality.o3IndexLevel.indexLevelName;
        }

        if(airQuality.pm10IndexLevel){
            params['pm10'] = airQuality.pm10IndexLevel.indexLevelName;
        }

        if(airQuality.pm25IndexLevel){
            params['pm25'] = airQuality.pm25IndexLevel.indexLevelName;
        }

        if(airQuality.so2IndexLevel){
            params['so2'] = airQuality.so2IndexLevel.indexLevelName;
        }

        if(airQuality.c6h6){
            params['c6h6'] = airQuality.so2IndexLevel.indexLevelName;
        }

        if(airQuality.stIndexLevel && airQuality.stIndexLevel.stIndexLevelName){
            params['st'] = airQuality.stIndexLevel.stIndexLevelName;
        }

        return params;
    }
}
